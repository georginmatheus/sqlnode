const express = require("express")
const routes = require("./routes")

require("./database")

const app = express()
//informa para o express que as requisições serão feitas no formato de json
app.use(express.json())
//Express usa as rotas definidas no arquivo routes.js
app.use(routes)

app.listen(3030)