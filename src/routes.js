const express = require("express")
const UserConstroller = require("./controllers/UserConstroller")
const AddressesController = require("./controllers/AddressesController")

const routes = express.Router()

routes.get("/", (req, res) => {
    return res.json({
        hello : "World"
    })
})

routes.get("/users", UserConstroller.index)
routes.post("/users", UserConstroller.store)

routes.post("/users/:user_id/addresses", AddressesController.store)
routes.get("/users/:user_id/addresses", AddressesController.index)
module.exports = routes