module.exports = {
    dialect : "postgres",
    host : "localhost",
    username : "postgres",
    password : "1723643000",
    database : "sqlnode",
    define : {
        timestamps : true,
        underscored : true,
    },
}

//TIMESTAMPS serve para informar que todas as tabelas do bd tenham os campos created_at e updated_at
//Esses campos irão armazenar as datas de quando os registros foram criados e arualizados pela ultima vez

//UNDESCORED define que os nomes das tabelas e colunas sejam no formato snake_case
//por padrão o sequelize usa o PascalCase