const Address = require("../model/Address")
const User = require("../model/User")

module.exports = {

    async index (req, res){
        const { user_id } = req.params

        const user = await User.findByPk(user_id, {
            include : {
                association : "addresses"
            }
        })
        if(!user){
            return res.status(400).json({ error: "User not Found"})
        }

        return res.json(user)

        
    },

    async store(req, res){
        const { city, street, state, number, zip_code} = req.body
        const { user_id } = req.params

        const user = await User.findByPk(user_id)

        if(!user){
            return res.status(400).json({ error: "User not Found"})
        }

        const address = await Address.create({ user_id, city, street, state, number, zip_code})

        return res.json(address)
    }
}