const {Model, DataTypes} = require('sequelize')

class User extends Model{
    //passa a conexão com o banco de dados
    static init(connection){
        super.init({
            name : DataTypes.STRING,
            email : DataTypes.STRING,
            password : DataTypes.STRING,
        },
        {
            //passando a conexão para o sequelize
            sequelize : connection
        })
    }

    static associate(models){
        //infomando para o sequelize que a tabela de user pode possuir vários endereços
        this.hasMany(models.Address, {foreignKey: "user_id", as : "addresses"})
    }
}

module.exports = User;