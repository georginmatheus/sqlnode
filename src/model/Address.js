const {Model, DataTypes} = require('sequelize');

class Address extends Model{
    //passa a conexão com o banco de dados
    static init(connection){
        super.init({
            street : DataTypes.STRING,
            number : DataTypes.INTEGER,
            city : DataTypes.STRING,
            state : DataTypes.STRING,
            zip_code : DataTypes.STRING,
        },
        {
            //passando a conexão para o sequelize
            sequelize : connection
        })
    }

    static associate(models){
        //informando para o sequelize a relação entre as tabelas user e Addresses
        this.belongsTo(models.User, {foreignKey: "user_id", as:"addresses"})
    }

}

module.exports = Address;