'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable('addresses', { 
      id: {
        type : Sequelize.INTEGER,
        primaryKey : true,
        allowNull : false,
        autoIncrement : true
      },

      user_id : {
        type : Sequelize.INTEGER,
        references : {
          model : "users",
          key : "id"
        },
        allowNull : false,
        //caso o id do usuário referenciado mude, essa mudança será refletida nos registros dessa tabela
        onUpdate : "CASCADE",
        //caso o usuário referenciado seja deletado, o registro dessa tabela que o referencia também é deletado
        //Outros valores possiveis para esse campo são:
        //RESTRICT: Impede a alteração/delete do usuário enquanto houver registros o referenciando
        //SET NULL: atribui valor nulo a referencia e não exclui ou altera o registro 
        onDelete : "CASCADE"
      },

      city : {
        type : Sequelize.STRING,
        allowNull : false,
      },

      state : {
        type : Sequelize.STRING,
        allowNull : false,
      },

      street : {
        type : Sequelize.STRING,
        allowNull : false,
      },

      zip_code : {
        type : Sequelize.STRING,
        allowNull : false
      },

      number : {
        type : Sequelize.INTEGER,
        allowNull : false
      },

      created_at : {
        type : Sequelize.DATE,
        allowNull : false
      },

      updated_at : {
        type : Sequelize.DATE,
        allowNull : false
      },
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable('addresses');
  }
};
